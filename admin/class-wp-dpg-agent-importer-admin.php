<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://digitalpropertygroup.com
 * @since      1.0.0
 *
 * @package    Wp_DPG_Agent_Importer
 * @subpackage Wp_DPG_Agent_Importer/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Wp_DPG_Agent_Importer
 * @subpackage Wp_DPG_Agent_Importer/admin
 * @author     Your Name <email@example.com>
 */
class Wp_DPG_Agent_Importer_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $wp_dpg_agent_importer    The ID of this plugin.
	 */
	private $wp_dpg_agent_importer;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $wp_dpg_agent_importer       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $wp_dpg_agent_importer, $version ) {

		$this->wp_dpg_agent_importer = $wp_dpg_agent_importer;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wp_DPG_Agent_Importer_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wp_DPG_Agent_Importer_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->wp_dpg_agent_importer, plugin_dir_url( __FILE__ ) . 'css/wp-dpg-agent-importer-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wp_DPG_Agent_Importer_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wp_DPG_Agent_Importer_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->wp_dpg_agent_importer, plugin_dir_url( __FILE__ ) . 'js/wp-dpg-agent-importer-admin.js', array( 'jquery' ), $this->version, false );

	}

}
