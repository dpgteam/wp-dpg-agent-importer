<?php

/**
 * Register and output options and settings for this plugin.
 *
 * @link       http://digitalpropertygroup.com
 * @since      1.0.0
 *
 * @package    Wp_DPG_Agent_Importer
 * @subpackage Wp_DPG_Agent_Importer/includes
 */
class Wp_DPG_Agent_Importer_Options {
    public static function wp_dpg_importer_settings_init() {
        // register a new setting for "wp_dpg_importer" page
        register_setting( 'wp_dpg_importer', 'wp_dpg_importer_options' );

        // register a new section in the "wp_dpg_importer" page
        add_settings_section(
            'wp_dpg_importer_section_options',
            __( 'DPG Importer', 'wp_dpg_importer' ),
            ['Wp_DPG_Agent_Importer_Options', 'wp_dpg_importer_section_options_cb'],
            'wp_dpg_importer'
        );

        // register a new field in the "wp_dpg_importer_section_options" section, inside the "wp_dpg_importer" page
        add_settings_field(
            'main_site_url', // as of WP 4.6 this value is used only internally
            // use $args' label_for to populate the id inside the callback
            __( 'Main Site URL', 'wp_dpg_importer' ),
            ['Wp_DPG_Agent_Importer_Options', 'wp_dpg_importer_field_main_site_cb'],
            'wp_dpg_importer',
            'wp_dpg_importer_section_options',
            [
                'label_for'                   => 'main_site_url',
                'class'                       => 'wp_dpg_importer_row',
                'wp_dpg_importer_custom_data' => 'custom',
            ]
        );
        add_settings_field(
            'agent_slug', // as of WP 4.6 this value is used only internally
            // use $args' label_for to populate the id inside the callback
            __( 'Main Agent Slug', 'wp_dpg_importer' ),
            ['Wp_DPG_Agent_Importer_Options', 'wp_dpg_importer_field_main_agent_cb'],
            'wp_dpg_importer',
            'wp_dpg_importer_section_options',
            [
                'label_for'                   => 'agent_slug',
                'class'                       => 'wp_dpg_importer_row',
                'wp_dpg_importer_custom_data' => 'custom',
            ]
        );
    }
    /**
     * Shows the settings header and user info.
     * @param  array $args
     * @return void
     */
    public static function wp_dpg_importer_section_options_cb( $args ) {
        ?>
            <p id="<?php echo esc_attr( $args['id'] ); ?>"><?php esc_html_e( "Connect this agent site to the main agency website to setup importer.", 'wp_dpg_importer' ); ?></p>
        <?php
    }
    /**
     * Outputs settings HTML for the site's Main URL setting.
     * @param  array $args
     * @return void
     */
    public static function wp_dpg_importer_field_main_site_cb( $args ) {
        $options = get_option( 'wp_dpg_importer_options' );
        ?>
            <div class="row">
                <input type="text" id="<?php echo esc_attr( $args['label_for'] ); ?>"
                    data-custom="<?php echo esc_attr( $args['wp_dpg_importer_custom_data'] ); ?>"
                    value="<?php echo $options['main_site_url'] ?? 'http://';?>"
                    name="wp_dpg_importer_options[<?php echo esc_attr( $args['label_for'] ); ?>]"
                >
                <p class="description">Enter the URL for the main agency WordPress site.</p>
            </div>
        <?php
    }
    /**
     * Outputs settings HTML for the site's Main Agent setting.
     * @param  array $args
     * @return void
     */
    public static function wp_dpg_importer_field_main_agent_cb( $args ) {
        $options = get_option( 'wp_dpg_importer_options' );
        ?>
            <div class="row">
                <input type="text" id="<?php echo esc_attr( $args['label_for'] ); ?>"
                    data-custom="<?php echo esc_attr( $args['wp_dpg_importer_custom_data'] ); ?>"
                    value="<?php echo $options['agent_slug'] ?? '';?>"
                    name="wp_dpg_importer_options[<?php echo esc_attr( $args['label_for'] ); ?>]"
                >
                <p class="description">Enter the slug for this site's pricipal agent.</p>
            </div>
        <?php
    }
    /**
     * Adds this the plugin settings to the Main WP Admin menu.
     * @return void
     */
    public static function wp_dpg_importer_options_page() {
        // add top level menu page
        add_menu_page(
            'DPG Importer',
            'DPG Importer',
            'manage_options',
            'wp_dpg_importer',
            ['Wp_DPG_Agent_Importer_Options', 'wp_dpg_importer_options_page_html']
        );
    }
    /**
     * Outer HTML for settings page.
     * @return void
     */
    public static function wp_dpg_importer_options_page_html() {
        // check user capabilities
        if ( ! current_user_can( 'manage_options' ) ) {
            return;
        }
        // add error/update messages

        // check if the user have submitted the settings
        // wordpress will add the "settings-updated" $_GET parameter to the url
        if ( isset( $_GET['settings-updated'] ) ) {
            // add settings saved message with the class of "updated"
            add_settings_error( 'wp_dpg_importer_messages', 'wp_dpg_importer_message', __( 'Settings Saved', 'wp_dpg_importer' ), 'updated' );
        }

        // show error/update messages
        settings_errors( 'wp_dpg_importer_messages' );
        ?>
            <div class="wrap">
            <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
            <form action="options.php" method="post">
        <?php
        // output security fields for the registered setting "wp_dpg_importer"
        settings_fields( 'wp_dpg_importer' );
        // output setting sections and their fields
        // (sections are registered for "wp_dpg_importer", each field is registered to a specific section)
        do_settings_sections( 'wp_dpg_importer' );
        // output save settings button
        submit_button( 'Save Settings' );
        ?>
            </form>
            </div>
        <?php
    }
}
