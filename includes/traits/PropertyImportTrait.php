<?php
trait PropertyImportTrait  {
    protected $properties_created     = 0;
    protected $properties_updated     = 0;
    protected $properties_actioned    = [];

    protected function createOrUpdateProperty($property) {
        if ( in_array($property->fields->id, $this->properties_actioned) ) {
            return;
        } else {
            $this->properties_actioned[] = $property->fields->id;
        }
        $fields = $property->fields;
        $title = '';
        if ($fields->address_street_number) {
            $title = trim(implode(' ', [(!empty($fields->address_sub_number) ? $fields->address_sub_number . '/' : '') . $fields->address_street_number, $fields->address_street . ',', $fields->address_suburb, $fields->address_postcode]), '-');
        }


        // Find existing post
        $found_post = new WP_Query([
            'posts_per_page' => 2,
            'post_type' => 'property',
            'meta_key' => 'id',
            'meta_value' => $property->fields->id,
            'post_status' => ['draft', 'pending', 'future', 'publish', 'private']
        ]);
        // Create post object
        $post = [
            'post_name'         => $property->fields->id,
            'post_title'        => $title,
            'post_content'      => $property->post_content,
            'post_type'         => 'property',
            'post_author'       => 1,
            'post_status'       => 'publish',
            'post_date'         => $property->fields->created_at,
            'post_date_gmt'     => get_gmt_from_date($property->fields->created_at),
            'post_modified'     => $property->fields->updated_at,
            'post_modified_gmt' => get_gmt_from_date($property->fields->updated_at),
        ];
        // If existing post found, set id
        if ($found_post->found_posts) {
            $post['ID'] = $found_post->posts[0]->ID;
            $this->properties_updated++;
        } else {
            $this->properties_created++;
        }
        // Create/Update post
        $post_id = wp_insert_post($post);
        $this->updatePropertyFields($property, $post_id);
        // Delete duplicates
        if ($found_post->found_posts >= 2) {
            wp_delete_post($found_post->posts[1]->ID, true);
        }
    }
    protected function updatePropertyFields($property, $post_id) {
        // Update ACF fields
        update_field('field_575d06c631d44', $property->fields->id, $post_id); // id
        update_field('field_57a57e53c15c3', $property->fields->branch_id, $post_id); // branch_id
        update_field('field_579d956ac7a19', 0, $post_id); // force_update
        update_field('field_579451188301d', $property->fields->headline, $post_id); // headline
        update_field('field_5738021c707f5', $property->fields->property_type, $post_id); // property_type
        update_field('field_575d1ab083c33', $property->fields->property_status, $post_id); // property_status
        update_field('field_575d213ee4873', $property->fields->video, $post_id); // video
        update_field('field_5794465801b89', $property->fields->address_full, $post_id); // address_full
        update_field('field_57c0f2f35e551', $property->fields->address_sub_number, $post_id); // address_sub_number
        update_field('field_574138d878576', $property->fields->address_street_number, $post_id); // address_street_number
        update_field('field_575d2389b3b52', $property->fields->address_street, $post_id); // address_street
        update_field('field_575d23a6b3b53', ucwords(strtolower($property->fields->address_suburb)), $post_id); // address_suburb
        update_field('field_575d23e4f01f8', $property->fields->address_state, $post_id); // address_state
        update_field('field_575d2430f01f9', $property->fields->address_postcode, $post_id); // address_postcode
        update_field('field_575d293c23468', $property->fields->price, $post_id); // price
        update_field('field_579d9503c7a18', $property->fields->price_display, $post_id); // price_display
        update_field('field_579d94eec7a17', $property->fields->price_view, $post_id); // price_view
        update_field('field_57d51b1334e73', $property->fields->under_offer, $post_id); // under_offer
        update_field('field_579444616bd70', $property->fields->lat, $post_id); // lat
        update_field('field_579444076bd6f', $property->fields->lon, $post_id); // lon
        update_field('field_575d2a4020ce6', $property->fields->auction_date, $post_id); // auction_date
        update_field('field_575d36111e1ec', $property->fields->created_at, $post_id); // created_at
        update_field('field_575d361a1e1ed', $property->fields->updated_at, $post_id); // updated_at
        update_field('field_579447a323d1d', $property->fields->floor_plan, $post_id); // floor_plan
        update_field('field_575d19a84d6da', $property->fields->category, $post_id); // category
        update_field('field_590688b1129b4', $property->fields->property_media, $post_id); // property_media

        $this->updatePropertyPricing($property, $post_id);
        $this->updatePropertyInspectionTimes($property, $post_id);
        $this->updatePropertyFeatureFields($property, $post_id);
        $this->updatePropertyImages($property, $post_id);
        $this->updatePropertyNeighbourhoods($property, $post_id);
        $this->updatePropertyAgents($property, $post_id);

    }
    /**
     * Updates property pricing data.
     * @param  Object $property
     * @param  Integer $post_id
     * @return void
     */
    protected function updatePropertyPricing($property, $post_id) {
        // Rent details
        if (!empty($property->property_rents[0])) {
            update_field('field_57944cb525333', $property->property_rents[0]->rent_amount ?: '', $post_id); // rent_amount
            update_field('field_57944cc125334', $property->property_rents[0]->rent_period ?: '', $post_id); // rent_period
        } elseif (!empty($property->commercial_rent)) {
            update_field('field_57944cb525333', $property->commercial_rent ?: '', $post_id); // rent_amount
            update_field('field_57944cc125334', $property->commercial_rent_period ?: '', $post_id); // rent_period
        }

        // Sold details
        if (!empty($property->property_sold_details[0])) {
            update_field('field_57944f1525335', $property->property_sold_details[0]->sold_date ?: '', $post_id); // sold_date
            update_field('field_57c129e7b76d2', $property->property_sold_details[0]->sold_price ?: '', $post_id); // sold_price
            update_field('field_57c12e26b76d3', $property->property_sold_details[0]->display_sold_price ?: '', $post_id); // display_sold_price
        }
    }
    /**
     * Updates property inspection time.
     * @param  Object $property
     * @param  Integer $post_id
     * @return void
     */
    protected function updatePropertyInspectionTimes($property, $post_id) {
        update_field('inspection_times', null, $post_id);
        if (!empty($property->fields->inspection_times)) {
            $times = [];
            foreach( $property->fields->inspection_times as $time ) {
                $time = [
                    'field_573818f7864ba' => $time->label, // label
                    'field_573819ed976d1' => $time->timestamp_start, // timestamp_start
                    'field_57837d77d0012' => $time->timestamp_end, // timestamp_end
                ];
                add_row('field_573809506125a', $time, $post_id);
            }
        }
    }
    /**
     * Extracts property features from $property and saves field values.
     * @param  StdClass $property
     * @param  integer $post_id
     * @return void
     */
    protected function updatePropertyFeatureFields($property, $post_id) {
        update_field('field_576a828cf7be8', $property->fields->bedrooms, $post_id);
        update_field('field_576a82a7f7be9', $property->fields->bathrooms, $post_id);
        update_field('field_576a82b6f7bea', $property->fields->garages, $post_id);
        update_field('field_576a82ddf7beb', $property->fields->carports, $post_id);
        // Compute Car Spaces
        update_field('field_5938ada702f02', $property->fields->carports + $property->fields->garages + $property->fields->open_spaces, $post_id);
    }
    /**
     * Updates property images.
     * @param  Object $property
     * @param  Integer $post_id
     * @return void
     */
    protected function updatePropertyImages($property, $post_id) {
        update_field('field_575d2d9a15839', null, $post_id);
        if (!empty($property->fields->images)) {
            foreach ($property->fields->images as $image) {
                add_row('field_575d2d9a15839', ['url' => $image->url], $post_id);
            }
        }
    }
    /**
     * Attaches the suburb post_id to this property. If a neighbourhood
     * doesn't exist locally, a stub is created.
     * @param  Object $property
     * @return void
     */
    protected function updatePropertyNeighbourhoods($property) {
        // Add suburb if it does not exist
        if (!empty($property->address_suburb) && !empty($property->address_state && $property->address_postcode)) {
            $found_suburb = get_posts(array(
                'numberposts' => 1,
                'post_type' => 'neighbourhood',
                'title' => ucwords(strtolower($property->address_suburb)),
                'post_status' => ['draft', 'pending', 'future', 'publish', 'private']
            ));
            if (!$found_suburb || $force_update) {
                // Create post object
                $suburb = [
                    'post_title' => ucwords(strtolower($property->address_suburb)),
                    'post_type' => 'neighbourhood',
                    'post_content' => $found_suburb[0]->post_content,
                    'post_author' => 1
                ];
                if (!$found_suburb) {
                    $this->suburbs_created++;
                    $suburb['post_status'] = 'publish';
                } else {
                    $suburb['ID'] = $found_suburb[0]->ID;
                    $suburb['post_status'] = $found_suburb[0]->post_status;
                }
                $suburb_id = wp_insert_post($suburb);
                // Update ACF fields
                update_field('field_57594f572f357', $property->address_postcode, $suburb_id); // postcode
                update_field('field_57594ff02f359', $property->address_state, $suburb_id); // state
            }
        }
    }
    /**
     * Parses property data and attaches the property's agents.
     * If an agent doesn't exist locally, a stub is created.
     * @param  Object $property
     * @param  Integer $post_id
     * @return void
     */
    protected function updatePropertyAgents($property, $post_id) {
        if ($property->fields->property_agent) {
            $agent_ids = [];
            foreach ($property->fields->property_agent as $agent) {
                $this->agent_post_names[] = $agent->post_name;
                $post = $this->getAgentByPostName($agent->post_name);
                if( ! $post ) {
                    // Create post object
                    $agent = [
                        'post_title'  => $agent->post_title,
                        'post_name'   => $agent->post_name,
                        'post_type'   => 'agent',
                        'post_status' => 'publish',
                        'post_author' => 1
                    ];
                    $agent_ids[] = wp_insert_post($agent);
                } else {
                    $agent_ids[] = $post->ID;
                }
            }
            update_field('field_573803c4d1ee0', $agent_ids, $post_id); // agents
        }
    }
}
