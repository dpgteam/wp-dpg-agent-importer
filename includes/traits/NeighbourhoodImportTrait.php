<?php
trait NeighbourhoodImportTrait  {
    /**
     * Updates existing $neighbourhood entry or creates a new one.
     * @param  Object $neighbourhood
     * @return void
     */
    protected function createOrUpdateNeighbourhood($neighbourhood) {
        $found_post = new WP_Query([
            'posts_per_page' => 2,
            'post_type'      => 'neighbourhood',
            'name'           => $neighbourhood->post_name,
            'post_status'    => ['draft', 'pending', 'future', 'publish', 'private']
        ]);

        $post = [
            'post_name'    => $neighbourhood->post_name,
            'post_title'   => $neighbourhood->post_title,
            'post_content' => $neighbourhood->post_content,
            'post_type'    => 'neighbourhood',
            'post_status'  => 'publish',
            'post_author'  => 1,
        ];
        // If existing post found, set id
        if ($found_post->found_posts) {
            $post['ID'] = $found_post->posts[0]->ID;
            $this->neighbourhoods_updated++;
        } else {
            $this->neighbourhoods_created++;
        }
        $post_id = wp_insert_post($post);

        $this->updateNeighbourhoodFields($neighbourhood->fields, $post_id);
        $this->updateNeighbourhoodFeaturedImageField($neighbourhood->fields, $post_id);
    }
    /**
     * Updates $neighbourhood's custom field data.
     * @param  Object $neighbourhood
     * @param  Integer $post_id
     * @return void
     */
    protected function updateNeighbourhoodFields($fields, $post_id) {
        update_field('field_57594f572f357', $fields->postcode, $post_id);
        update_field('field_57594ff02f359', $fields->state, $post_id);
        update_field('field_57ac80e427fe9', $fields->social_eazie_widget_id, $post_id);
        update_field('field_5774f888f00ac', $fields->lifestyle_intro_content, $post_id);
        update_field('field_5774f8aef00ad', $fields->people_intro_content, $post_id);
        update_field('field_5774f8b1f00ae', $fields->homes_intro_content, $post_id);
        update_field('field_594386c58135e', $fields->thumbnail_image, $post_id);
        update_field('field_5949f9af042da', $fields->discover_content_group, $post_id);
    }
    protected function updateNeighbourhoodFeaturedImageField($fields, $post_id) {
        $data = [];
        foreach ( $fields->featured_image as $image ) {
            $data[] = [
                'field_599404cc022d8' => $image->size, // size
                'field_599404d6022d9' => $image->width, // width
                'field_599404e1022da' => $image->height, // height
                'field_599404e9022db' => $image->url, // url
            ];
        }
        update_field('field_599404bd022d7', $data, $post_id); //featured_image'
    }
    /**
     * Queries property data for distinct suburb values and
     * returns an array of neighbourhood post names.
     * @return Array
     */
    protected function getSuburbsFromProperties() {
        global $wpdb;
        $results = $wpdb->get_col( $wpdb->prepare( "
                SELECT DISTINCT pm.meta_value FROM {$wpdb->postmeta} pm
                LEFT JOIN {$wpdb->posts} p ON p.ID = pm.post_id
                WHERE pm.meta_key = 'address_suburb'
                AND p.post_type = 'property'
            ", 'address_suburb', 'publish', 'property' ) );

        foreach( $results as $key => $result ) {
            $results[$key] = sanitize_title_for_query( $result );
        }
        return $results;
    }
    /**
     * If there aren't any properties for a neighbourhood, it is set to 'Draft'.
     * @return void
     */
    protected function hideUnusedNeighbourhoods() {
        $suburbs = $this->getSuburbsFromProperties();
        $neighbourhoods = new WP_Query([
            'posts_per_page' => -1,
            'post_type'      => 'neighbourhood',
        ]);
        // var_dump($neighbourhoods->posts); die;
        foreach($neighbourhoods->posts as $neigbourhood) {
            if( ! in_array($neigbourhood->post_name, $suburbs)) {
                $neigbourhood->post_status = 'draft';
                wp_update_post($neigbourhood);
            }
        }
    }
}
