<?php
trait AgentImportTrait  {
    protected function createOrUpdateAgent($property_agent) {
        $title = ucwords($property_agent->post_title);
        if ( ! $title ) return;
        $found_agent = get_posts(array(
            'numberposts' => 1,
            'post_type' => 'agent',
            'post_status' => ['draft', 'pending', 'future', 'publish', 'private'],
            'title' => $title
        ));
        // Create post object
        $agent = [
            'post_title' => $title,
            'post_type' => 'agent',
            'post_author' => 1,
        ];

        // If existing post found, set id
        if ($found_agent) {
            $agent['ID'] = $found_agent[0]->ID;
            $this->agents_updated++;
            $agent['post_status']  = $found_agent[0]->post_status;
        } else {
            $this->agents_created++;
            $agent['post_status'] = 'publish';
        }
        $agent['post_content'] = $property_agent->post_content;

        // Create/Update agent
        $agent_post_id = wp_insert_post($agent);
        $this->updateAgentFields($property_agent->fields, $agent_post_id);
        $this->updateAgentFeaturedImageField($property_agent->fields, $agent_post_id);
        $this->updateAgentOFficeField($property_agent, $agent_post_id);
        return $agent_post_id;
    }
    /**
     * Updates $agent's general custom fields.
     * @param  StdClass $fields
     * @param  integer $post_id
     * @return void
     */
    protected function updateAgentFields($fields, $post_id) {

        // Update ACF fields
        update_field( 'field_5777bc707d486', $fields->title, $post_id ); // title
        update_field( 'field_576f495abea01', $fields->id, $post_id ); // agent_id
        update_field( 'field_57443b884937b', $fields->mobile, $post_id ); // mobile
        update_field( 'field_57443bae4937c', $fields->phone, $post_id ); // phone
        update_field( 'field_576f48c7c6c04', $fields->created_at, $post_id ); // created_at
        update_field( 'field_576f48cfc6c05', $fields->updated_at, $post_id ); // updated_at
        update_field( 'field_57443b794937a', $fields->email, $post_id );
        update_field( 'field_577ba263b236a', $fields->video_type, $post_id);  // Video Type
        update_field( 'field_577ba2b82dce5', $fields->video_id, $post_id); // Video ID
        update_field( 'field_594375a8bd9db', json_encode($fields->photo), $post_id); // Video ID
        update_field( 'field_57ac80e427fe9', $fields->social_eazie_widget_id, $post_id); // social_eazie_widget_id
        update_field( 'field_598aa0652b5b9', $fields->local_video, $post_id); //local_video
        update_field( 'field_5994f5b8c443a', $fields->trust_pilot_tags, $post_id ); // trust_pilot_tags

        $this->updateTestimonialField($fields, $post_id);
        // additional_videos
        if ( ! empty($fields->additional_videos) ) {
            update_field('additional_videos', null, $post_id); // Clear existing data and update
            foreach($fields->additional_videos as $video) {
                $video = [
                    'field_57e09426cf912' => $video->video_type,
                    'field_57e09464cf913' => $video->video_id,
                ];
                add_row('field_57e09406cf911', $video, $post_id); // 'additional_videos'
                // add_row('additional_videos', $data, $post_id);
            }
        }
    }
    /**
     * Updates $agent's featured image custom field with
     * images received.
     * @param  StdClass $fields
     * @param  integer $post_id
     * @return void
     */
    protected function updateAgentFeaturedImageField($fields, $post_id) {
        $data = [];
        foreach($fields->featured_image as $key => $image) {
            $data[] = [
                'field_59927634d1b74' => $key, // size
                'field_59927726d1b75' => $image->width, // width
                'field_59927732d1b76' => $image->height, // height
                'field_5992773fd1b77' => $image->url, // url
            ];
            update_field('field_59927608d1b73', $data, $post_id);
        }
    }
    /**
     * Updates $agent's offices custom field with offices received.
     * @param  StdClass $fields
     * @param  integer $post_id
     * @return void
     */
    protected function updateAgentOfficeField($fields, $post_id) {
        $offices = [];
        foreach($fields->offices as $key => $office) {
            $offices[$key] = [
                'field_597e93770a9b8' => $office->post_title, // title
                'field_597e938f0a9b9' => $office->post_name, // slug
                'field_597ea1e2d0bcf' => [], // image
                'field_5983e51cb7cf0' => [   // location
                    'field_5983e528b7cf1' => $office->location->address,  // address
                    'field_5983e55cb7cf2' => $office->location->lat,  // lat
                    'field_5983e56db7cf3' => $office->location->lng,  // lng
                ],
            ];
            foreach(get_object_vars($office->image) as $size => $image) {
                $offices[$key]['field_597ea1e2d0bcf'][] = [
                    'field_597ea205d0bd0' => $size, // size
                    'field_597ea233d0bd1' => $image->width, // width
                    'field_597ea23bd0bd2' => $image->height, // height
                    'field_597ea241d0bd3' => $image->url, // url

                ];
            }
        }
        update_field('field_597e93610a9b7', $offices, $post_id); // Offices
    }
    /**
     * Updates $agent's testimonials custom field with testimonials received.
     * @param  StdClass $fields
     * @param  integer $post_id
     * @return void
     */
    protected function updateTestimonialField($fields, $post_id) {
        update_field('field_57443ccd4937d', null, $post_id); // Testimonials group
        foreach( $fields->testimonials as $testimonial ) {
            $testimonial = [
                "field_57443db34937e" => $testimonial->client, // Client
                "field_57443dc64937f" => $testimonial->details, // Details
            ];
            add_row('field_57443ccd4937d', $testimonial, $post_id); // Testimonials group
        }
    }
    /**
     * Gets the agent post with $post_name.
     * @param  string $post_name
     * @return WP_Post
     */
    protected function getAgentByPostName($post_name) {
        return get_posts([
            'name'           => $post_name,
            'post_type'      => 'agent',
            'post_status'    => 'publish',
            'posts_per_page' => 1
        ])[0] ?? null;
    }
}
