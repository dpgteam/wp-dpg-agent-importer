<?php
/**
 * Registers the Custom Post Types required for a single agent site.
 * @return void
 */
function cptui_register_my_cpts() {
    /**
     * Post Type: Agents.
     */
    $labels = array(
        "name" => __( "Agents", "" ),
        "singular_name" => __( "Agent", "" ),
    );

    $args = array(
        "label" => __( "Agents", "" ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "agent", "with_front" => false ),
        "query_var" => true,
        "supports" => array( "title", "editor", "thumbnail" ),
    );

    register_post_type( "agent", $args );

    /**
     * Post Type: Properties.
     */

    $labels = array(
        "name" => __( "Properties", "" ),
        "singular_name" => __( "Property", "" ),
    );

    $args = array(
        "label" => __( "Properties", "" ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "property", "with_front" => false ),
        "query_var" => true,
        "supports" => array( "title", "editor", "thumbnail" ),
    );

    register_post_type( "property", $args );

    /**
     * Post Type: Neighbourhoods.
     */

    $labels = array(
        "name" => __( "Neighbourhoods", "" ),
        "singular_name" => __( "Neighbourhood", "" ),
    );

    $args = array(
        "label" => __( "Neighbourhoods", "" ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "neighbourhood", "with_front" => false ),
        "query_var" => true,
        "supports" => array( "title", "editor", "thumbnail" ),
    );

    register_post_type( "neighbourhood", $args );

    /**
     * Post Type: Resources.
     */

    $labels = array(
        "name" => __( "Resources", "" ),
        "singular_name" => __( "Resource", "" ),
    );

    $args = array(
        "label" => __( "Resources", "" ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "resource", "with_front" => false ),
        "query_var" => true,
        "supports" => array( "title", "editor", "thumbnail" ),
    );

    register_post_type( "resource", $args );

    /**
     * Post Type: Testimonials.
     */

    $labels = array(
        "name" => __( "Testimonials", "" ),
        "singular_name" => __( "Testimonial", "" ),
        "menu_name" => __( "Testimonials", "" ),
        "all_items" => __( "All Testimonials", "" ),
        "add_new" => __( "Add new Testimonial", "" ),
        "add_new_item" => __( "Add new Testimonial", "" ),
        "edit_item" => __( "Edit Testimonial", "" ),
        "new_item" => __( "New Testimonial", "" ),
        "view_item" => __( "View Testimonial", "" ),
        "view_items" => __( "View Testimonials", "" ),
        "search_items" => __( "Search Testimonials", "" ),
        "not_found" => __( "No Testimonials found", "" ),
        "not_found_in_trash" => __( "No Testimonials found in Trash", "" ),
        "featured_image" => __( "Featured Image for this Testimonials", "" ),
        "set_featured_image" => __( "Set Featured Image for this Testimonials", "" ),
        "remove_featured_image" => __( "Remove Featured Image for this Testimonials", "" ),
        "use_featured_image" => __( "Use as Featured Image for this Testimonials", "" ),
        "archives" => __( "Testimonial Archives", "" ),
        "filter_items_list" => __( "Filter Testimonials", "" ),
        "items_list_navigation" => __( "Testimonial List Navigation", "" ),
        "items_list" => __( "Testimonial List", "" ),
        "attributes" => __( "Testimonial Attributes", "" ),
    );

    $args = array(
        "label" => __( "Testimonials", "" ),
        "labels" => $labels,
        "description" => "Client testimonials for this agent.",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => true,
        "rest_base" => "testimonial",
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => true,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "testimonial", "with_front" => true ),
        "query_var" => true,
        "menu_position" => 30,
        "supports" => array( "title", "editor", "thumbnail" ),
    );

    register_post_type( "testimonial", $args );

    /**
     * Post Type: Agents.
     */

    $labels = array(
        "name" => __( "Agents", "" ),
        "singular_name" => __( "Agent", "" ),
    );
}

add_action( 'init', 'cptui_register_my_cpts' );
?>
