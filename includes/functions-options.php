<?php
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page([
        'page_title'    => 'DPG Agent Importer',
        'menu_title'    => 'DPG Importer',
        'menu_slug'     => 'dpg-agent-importer',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ]);
    update_field('agent_slug', getenv('DPG_AGENT_SLUG') ?? '', 'option');
    update_field('main_site', getenv('DPG_AGENT_MAIN_SITEURL') ?? '', 'option');
}

?>
