<?php
/**
Controller name: DPG Agent Importer
Controller description: Synchronises a single agent's data with the main agency WordPress site.
 */

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
require(__DIR__ . '/traits/AgentImportTrait.php');
require(__DIR__ . '/traits/AttachmentImportTrait.php');
require(__DIR__ . '/traits/NeighbourhoodImportTrait.php');
require(__DIR__ . '/traits/PropertyImportTrait.php');
/**
 * DPG Agent Import Controller
 *
 * @link       http://digitalpropertygroup.com
 * @since      1.0.0
 *
 * @package    Wp_DPG_Agent_Importer
 * @subpackage Wp_DPG_Agent_Importer/includes
 */

/**
 * DPG Agent Import Controller
 *
 * This class defines endpoints for retrieving an agent's property data from a main WP install.
 *
 * @since      1.0.0
 * @package    Wp_DPG_Agent_Importer
 * @subpackage Wp_DPG_Agent_Importer/includes
 * @author     Paul Beynon <paul@digitalpropertygroup.com>
 */
/*
 Controller name: DPG Agent Property Importer
 Controller description: Adds an endpoint to synchronise and agent's property data with an agency WordPress install.
 */
class JSON_API_Dpg_Agent_Import_Controller {
    use AgentImportTrait;
    use AttachmentImportTrait;
    use NeighbourhoodImportTrait;
    use PropertyImportTrait;

    protected $agent_slug = '';
    protected $agents_created         = 0;
    protected $agents_updated         = 0;
    protected $neighbourhoods_created = 0;
    protected $neighbourhoods_updated = 0;
    /* A list of post_names for agents which have already been updated during this import. */
    protected $agent_post_names = [];
	/* How many properties to get per request. */
	protected $posts_per_page = 100;
    /* Stores plugin options from the WP database. */
    protected $options;
	/* Parameters passed with GET request. */
	protected $params = [];
	/* An array of agent post data. */
	protected $agent = [];
	/* An array of this agent's property data. */
	protected $properties = [];
    /* The number of pages to offset results by. */
    protected $offset = 0;
	/**
	 * Stores query parameters to class.
	 */
	public function __construct() {
        $this->options = get_option( 'wp_dpg_importer_options' );
        $this->options = [
            'main_site_url' => get_field('main_site', 'option'),
            'agent_slug'    => get_field('agent_slug', 'option'),
        ];
        $this->offset  = get_option( 'wp_dpg_importer_query_offset' ) ? get_option( 'wp_dpg_importer_query_offset' ) : 0;
        $this->params();
	}
    /**
     * Parses request query string to array.
     * @return array
     */
    protected function params() {
        if ( ! $this->params ) {
            parse_str($_SERVER['QUERY_STRING'], $params);
            $this->params = $params;
            $this->params['offset'] = $this->params['offset'] ?? 0;
        }
        return $this->params;
    }
    protected function base_uri() {
        return $this->options['wp_dpg_importer_main_site_url'];
    }
    public function import() {
        try {
            /* Make request. */
            $this->get_agent_properties();
            // Update additional agents here
            foreach(array_unique($this->agent_post_names) as $agent) {
                if ( $agent !== $this->options['agent_slug']) {
                    $this->get_agent_properties($agent);
                }
            }

            /* Update Neighbourhood Posts. */

            /* Update or reset import offset. */
            if ( $this->properties_created || $this->properties_updated ) {
                update_option('wp_dpg_importer_query_offset', $this->offset + 1);
            }  else {
                update_option('wp_dpg_importer_query_offset', 0);
            }
            return [
                'result'             => 'success',
                'properties_created' => $this->properties_created,
                'properties_updated' => $this->properties_updated,
                'agents_created'     => $this->agents_created,
                'agents_updated'     => $this->agents_updated,
            ];
        } catch (Exception $e) {
            return [
                'code'    => $e->getCode(),
                'message' => $e->getMessage()
            ];
        }
    }
    /**
     * Queries the main site for this agent's data and properties, or pass in an
     * agent slug to request that agent's data with no properties.
     * @param  string  $agent_slug
     * @return void
     */
	protected function get_agent_properties($agent_slug = null) {
        $client = new Client([
            'base_uri' => $this->options['main_site_url'] . '/api/dpg_agent_export/export/',
        ]);
        $properties = $agent_slug ? false : true;
        $agent_slug = $agent_slug ?? $this->options['agent_slug'];
        $query = [
            'offset'     => $this->offset,
            'agent_slug' => $agent_slug,
            'properties' => $properties,
        ];

        $response = $client->request('GET', '', [
            'query' => $query
        ]);
        $data = json_decode($response->getBody()->getContents());

        // Update main agent data.
        $this->createOrUpdateAgent($data->result);

        // Update or Create main agent's properties.
        if( $agent_slug === $this->options['agent_slug'] ) {
            $properties = $data->result->properties;
            foreach ( $properties as $property ) {
                $this->createOrUpdateProperty($property);
            }
        }
        return $response->getBody()->getContents();
    }
    public function get_neighbourhoods() {
        $client = new Client([
            'base_uri' => $this->options['main_site_url'] . '/api/dpg_agent_export/export_neighbourhoods/',
        ]);
        $query = [
            'neighbourhoods' => $this->getSuburbsFromProperties(),
        ];
        $response = $client->request('GET', '', [
            'query' => $query
        ]);
        $data = json_decode($response->getBody()->getContents());
        foreach( $data->neighbourhoods as $neighbourhood) {
            $this->createOrUpdateNeighbourhood($neighbourhood);
        }
        $this->hideUnusedNeighbourhoods();
        return [
            'result'                 => 'success',
            'neighbourhoods_created' => $this->neighbourhoods_created,
            'neighbourhoods_updated' => $this->neighbourhoods_updated,
        ];
    }

}
